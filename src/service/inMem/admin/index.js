import {requests} from "../group/sendJoinRequest";

export const loadJoinRequests = async () => requests;

export const sendJoinAcceptance = async (admin, group, person) => {
    console.log(`${admin.name} accepted ${person.name} to join ${group.uri}.`)
}

export const sendJoinRejection = async (admin, group, person) => {
    console.log(`${admin.name} rejected ${person.name} to join ${group.uri}.`)
}

export const deleteJoinRequest = async ({uri}) => {
    const request = requests.find(it => it.uri === uri)
    const index = requests.indexOf(request);
    requests.splice(index, 1);
    console.log(`deleted request ${request.uri}`)
}

const members = []

export const addToGroup = async (person, group) => {
    members.push(person)
    console.log({members})
}