export const persons = {
  "https://alice.example": {
    webId: "https://alice.example",
    name: "Alice",
  },
  "https://bob.example": {
    webId: "https://bob.example",
    name: "Bob",
  },
};

export default async (webId) =>
  Promise.resolve(persons[webId]);
