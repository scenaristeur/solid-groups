import { sendJoinRequest } from "./sendJoinRequest";
import fileClient from "../file-client";

jest.mock("../file-client", () => ({
  postFile: jest.fn(),
}));

describe("send join request", () => {
  it("posts a file", async () => {
    const person = {
      webId: "https://john.example/profile/card#me",
      name: "John",
    };
    const group = {
      uri: "https://group.example/groups/main#we",
      inbox: "https://group.example/groups/inbox/1337",
    };
    await sendJoinRequest(group, person);
    expect(fileClient.postFile).toHaveBeenCalledWith(
      "https://group.example/groups/inbox/join-offer",
      `
@prefix :      <#> .
@prefix as:    <https://www.w3.org/ns/activitystreams#> .

:it
    a as:Offer ;
    as:actor <https://john.example/profile/card#me> ;
    as:object :join ;
    as:summary "John asks to join the group" ;
    as:target <https://group.example/groups/main#we> .

:join
    as:actor <https://john.example/profile/card#me> ;
    as:object <https://group.example/groups/main#we> ;
    as:summary "John joins group" ;
    as:type as:Join .`,
      "text/turtle",
      { createPath: false }
    );
  });
});
