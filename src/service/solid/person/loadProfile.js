import data from "@solid/query-ldflex";

export default async (webId) => {
  const name = await data[webId].vcard_fn.value;
  const inbox = await data[webId].ldp_inbox.value;
  const imageSrc = await data[webId].vcard_hasPhoto.value;
  return { webId, name, inbox, imageSrc };
};
