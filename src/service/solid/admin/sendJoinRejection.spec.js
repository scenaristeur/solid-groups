import fileClient from "../file-client";

import sendJoinRejection from "./sendJoinRejection";

jest.mock("../file-client", () => ({
  postFile: jest.fn(),
}));

describe("send join rejection", () => {
  it("posts a file", async () => {
    const admin = {
      webId: "https://jane.example/profile/card#me",
      name: "Jane",
    };
    const person = {
      webId: "https://john.example/profile/card#me",
      inbox: "https://john.example/inbox/",
      name: "John",
    };
    const group = {
      uri: "https://group.example/groups/main#we",
      name: "VIP only",
      inbox: "https://group.example/groups/inbox/1337",
    };
    await sendJoinRejection(admin, group, person);
    expect(fileClient.postFile).toHaveBeenCalledWith(
      "https://john.example/inbox/rejection",
      `
@prefix :      <#> .
@prefix as:    <https://www.w3.org/ns/activitystreams#> .

:it
    a as:Reject ;
    as:actor <https://jane.example/profile/card#me> ;
    as:object :join ;
    as:summary "Jane rejected your join request" .

:join
    as:actor <https://john.example/profile/card#me> ;
    as:object <https://group.example/groups/main#we> ;
    as:summary "John joins group" ;
    as:type as:Join .`,
      "text/turtle",
      { createPath: false }
    );
  });

  it('throws error when person has no inbox',async () => {
    const admin = {
      webId: "https://jane.example/profile/card#me",
      name: "Jane",
    };
    const person = {
      webId: "https://john.example/profile/card#me",
      name: "John",
    };
    const group = {
      uri: "https://group.example/groups/main#we",
      name: "VIP only",
      inbox: "https://group.example/groups/inbox/1337",
    };
    const promise = sendJoinRejection(admin, group, person);
    await expect(promise).rejects.toThrow(new Error("The inbox of the person to reject is unknown."))
  });
});
