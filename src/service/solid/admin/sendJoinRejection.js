import fileClient from "../file-client";

const rejectToJoin = (admin, group) => (person) => `
@prefix :      <#> .
@prefix as:    <https://www.w3.org/ns/activitystreams#> .

:it
    a as:Reject ;
    as:actor <${admin.webId}> ;
    as:object :join ;
    as:summary "${admin.name} rejected your join request" .

:join
    as:actor <${person.webId}> ;
    as:object <${group.uri}> ;
    as:summary "${person.name} joins group" ;
    as:type as:Join .`;

export default async (admin, group, person) => {
  if (!person.inbox) {
    throw new Error ("The inbox of the person to reject is unknown.");
  }
  const url = new URL("rejection", person.inbox).href;
  const body = rejectToJoin(admin, group);
  return fileClient.postFile(url, body(person), "text/turtle", {createPath: false});
}
