import { v4 as uuidv4 } from "uuid";

import data from "@solid/query-ldflex";
import loadPotentialGroup from "./loadPotentialGroup";

const vcardGroup = data["http://www.w3.org/2006/vcard/ns#Group"];

export const createGroup = (uri) => data[uri].type.add(vcardGroup);

export const createInbox = (uri) =>
  data[uri].ldp_inbox.set(data[`./inbox/${uuidv4()}/`]);

export { loadPotentialGroup };