import * as auth from "solid-auth-client";

const popupUri = "https://solid.community/common/popup.html";

export default async () => {
  let session = await auth.currentSession();
  if (!session) {
    session = await auth.popupLogin({ popupUri });
  }
  return {
    webId: session.webId,
  };
};
