import Container from "@material-ui/core/Container";
import {Header} from "./group/header/Header";
import {Footer} from "./group/Footer";
import React from "react";

export const Layout = ({ children }) => {
    return (
        <>
            <Container maxWidth="lg">
                <Header title="Solid Groups" />
                <main>
                    {children}
                </main>
            </Container>
            <Footer title="Solid Groups" description="The Web is social." />
        </>
    );
};