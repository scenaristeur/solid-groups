import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import React from "react";
import {useSession} from "../authentication";

export const Logout = () => {
    const {user, logout} = useSession();
    return (
        <Grid container>
            <Grid item xs>
                <Link href="#" onClick={() => logout()} variant="body2">
                    Log out
                </Link>
            </Grid>
            <Grid item>({user.webId})</Grid>
        </Grid>
    );
};
