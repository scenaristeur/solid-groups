import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import {Hero} from "./Hero";
import {Main} from "./Main";
import {Sidebar} from "./Sidebar";
import {useGroup} from "./useGroup";
import LinearProgress from "@material-ui/core/LinearProgress";
import {PageError} from "../error/PageError";
import {Layout} from "../Layout";

const useStyles = makeStyles((theme) => ({
    mainGrid: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
}));

export const PageLoading = () => <LinearProgress color="secondary"/>;

export const GroupPage = ({group}) => {
    const classes = useStyles();
    return (
        <Layout>
            <Hero title={group.name}/>
            <Grid container spacing={5} className={classes.mainGrid}>
                <Main title="Members"/>
                <Sidebar title="" description="" group={group}/>
            </Grid>
        </Layout>
    );
};

export const Group = ({uri}) => {
    const {group, loading, error} = useGroup(uri);

    if (loading) return <PageLoading/>;
    if (error) return <PageError error={error}/>;

    return <GroupPage group={group}/>;
};
