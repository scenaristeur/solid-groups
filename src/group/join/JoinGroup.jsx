import React from "react";
import GroupAddIcon from "@material-ui/icons/GroupAdd";
import Button from "@material-ui/core/Button";
import {useSession} from "../../authentication";
import {LogInToJoin} from "./LogInToJoin";
import {useJoinRequest} from "./useJoinRequest";

export function JoinGroup({ group }) {
  const { loggedIn } = useSession();
  const { joinRequested } = useJoinRequest(group);
  return loggedIn ? (
    <>
      <Button
        onClick={() => joinRequested()}
        startIcon={<GroupAddIcon />}
        variant="contained"
        color="secondary"
      >
        Join Group
      </Button>
    </>
  ) : (
    <LogInToJoin />
  );
}
