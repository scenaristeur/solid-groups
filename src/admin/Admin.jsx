import {Layout} from "../Layout";
import React from "react";
import {JoinRequests} from "./JoinRequests";

export const Admin = ({uri}) => {
    return <Layout>
        <JoinRequests group={{uri}} />
    </Layout>
}